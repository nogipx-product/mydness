import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:loggable_mixin/loggable_mixin.dart';


extension on Tween {
  double progress(double value) {
    return (value - begin) / (end - begin);
  }
}


class FlipCube extends StatefulWidget {

  final Size size;
  final Duration flipDuration;

  final Widget front;
  final Widget bottom;

  final Curve curve;
  final bool debug;

  final int perspective;

  const FlipCube({Key key,
    @required this.size,
    this.flipDuration,
    this.front,
    this.bottom,
    this.curve = Curves.easeIn,
    this.debug = false,
    this.perspective = 1
  }): assert(size != null),
      super(key: key);

  @override
  _FlipCubeState createState() => _FlipCubeState();
}

class _FlipCubeState extends State<FlipCube> with TickerProviderStateMixin, Loggable {

  Matrix4 get perspectiveMatrix => Matrix4.identity()
    ..setEntry(3, 2, 0.001 * widget.perspective);

  AnimationController _animController;
  Animation _anim;

  final _zero = 0.0001;

  Tween pi2Tween;

  Color bottomColor;
  Color frontColor;

  @override
  void initState() {
    pi2Tween = Tween(begin: _zero, end: math.pi / 2);
    _animController = AnimationController(
      duration: widget.flipDuration ?? Duration(seconds: 5),
      vsync: this,
    )..repeat();
    super.initState();
    _anim = pi2Tween.animate(CurvedAnimation(
      curve: widget.curve,
      parent: _animController
    ));

    bottomColor = widget.debug ? Colors.green.withOpacity(.5) : Colors.transparent;
    frontColor = widget.debug ? Colors.blue.withOpacity(.5) : Colors.transparent;
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _anim,
      builder: (context, widget) {
        return _flipVertical(_anim.value, 0);
      },
    );
  }

  _flipVertical(double angle, double animationValue) {
    final frontTransform = perspectiveMatrix
      ..rotateX(angle * -1);

    final bottomTransform = perspectiveMatrix
      ..rotateX(math.pi / 2)
      ..rotateX(angle * -1);

    final size = widget.size;
    final progress = pi2Tween.progress(_anim.value);

//    final frontHeight = size.height - progress * size.height + 70 * progress;
//    final bottomHeight = progress * size.height + 70 - 70 * progress;

    final frontHeight = size.height - progress * size.height;
    final bottomHeight = progress * size.height;

    log.d("Progress ${(progress*100).toInt().toString().padLeft(3)} // Front ${frontHeight.toInt()} // Bottom ${bottomHeight.toInt()}");

    return Container(
      width: size.width,
      height: size.height,
      alignment: Alignment.center,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            height: frontHeight,
            width: size.width,
            child: Transform(
              transform: frontTransform,
              alignment: FractionalOffset.topCenter,
              child: Container(
                color: frontColor,
                child: widget.front ?? Center(child: Text("Bottom"))
              )
            ),
          ),
          Positioned(
            bottom: 0,
            height: bottomHeight,
            width: size.width,
            child: Transform(
              transform: bottomTransform,
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                color: bottomColor,
                child: widget.bottom ?? Center(child: Text("Bottom"))
              )
            ),
          )
        ],
      ),
    );
  }
}
