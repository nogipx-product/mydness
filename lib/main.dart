import 'package:auto_route/auto_route.dart';
import 'package:clean_architecture/clean_architecture.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cross_platform/flutter_cross_platform.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loggable_mixin/loggable_mixin.dart';
import 'package:mydness/config/ui.dart';
import 'package:mydness/router/device_router.gr.dart';
import 'package:mydness/router/web_router.gr.dart';


void main() {
  runApp(MydnessApp());
}

// ignore: must_be_immutable
class MydnessApp
  extends StatelessWidget
  with PlatformSplitSingletonMixin, Loggable {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Mydness",
      theme: ThemeData(
        primaryColor: MydnessColor.jet,
        backgroundColor: MydnessColor.jet,
        scaffoldBackgroundColor: MydnessColor.jet,
        accentColor: MydnessColor.flame,
        hoverColor: MydnessColor.flame.hover,
        splashColor: MydnessColor.flame.pressed,
        textTheme: GoogleFonts.robotoSlabTextTheme().apply(
          bodyColor: MydnessColor.mintCream,
          displayColor: MydnessColor.mintCream
        ),
        primaryTextTheme: GoogleFonts.londrinaSketchTextTheme().apply(
          bodyColor: MydnessColor.flame,
          displayColor: MydnessColor.flame,
        ),
        accentTextTheme: GoogleFonts.alegreyaSansTextTheme().apply(
          bodyColor: MydnessColor.mintCream,
          displayColor: MydnessColor.mintCream,
        )
      ),
      builder: (context, child) {
        initPlatforms(context);
        return CleanApplication(
          configuration: MydnessConfig(
            child: platformVariant
          )
        );
      },
    );
  }

  @override
  Navigator web(context) {
    return ExtendedNavigator(
      router: WebRouter(),
    );
  }

  @override
  Navigator device(context) {
    return ExtendedNavigator(
      router: DeviceRouter(),
    );
  }
}



class MydnessConfig extends Configuration {
  final Widget child;

  MydnessConfig({this.child}) : super(child: child);

  @override
  List<Factory<Controller>> controllers() {
    return [];
  }

  @override
  List<Implementation<Repository>> repositories() {
    return [];
  }

  @override
  List<Factory<Service>> services() {
    return [];
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  @override
  List<UseCase> usecases() {
    return [];
  }
  
}