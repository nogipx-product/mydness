
import 'package:auto_route/auto_route_annotations.dart';
import 'package:mydness/screen/bio_screen.dart';
import 'package:mydness/screen/test.dart';
import 'package:mydness/screen/web/welcome_screen.dart';

@MaterialAutoRouter(
  generateNavigationHelperExtension: true,
  generateRouteList: true
)
class $WebRouter {

  @initial
  WelcomeScreen welcomeScreen;

  BioScreen bioScreen;
  TestPage testPage;
}