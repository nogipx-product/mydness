// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:mydness/screen/bio_screen.dart';
import 'package:mydness/screen/test.dart';

abstract class Routes {
  static const bioScreen = '/';
  static const testPage = '/test-page';
  static const all = [
    bioScreen,
    testPage,
  ];
}

class DeviceRouter extends RouterBase {
  //This will probably be removed in future versions
  //you should call ExtendedNavigator.ofRouter<Router>() directly
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<DeviceRouter>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.bioScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => BioScreen(),
          settings: settings,
        );
      case Routes.testPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => TestPage(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Navigation helper methods extension
//***************************************************************************

extension DeviceRouterNavigationHelperMethods on ExtendedNavigatorState {
  Future pushBioScreen() => pushNamed(Routes.bioScreen);
  Future pushTestPage() => pushNamed(Routes.testPage);
}
