
import 'package:auto_route/auto_route_annotations.dart';
import 'package:mydness/screen/bio_screen.dart';
import 'package:mydness/screen/test.dart';

@MaterialAutoRouter(
  generateNavigationHelperExtension: true,
  generateRouteList: true
)
class $DeviceRouter {

  @initial
  BioScreen bioScreen;
  TestPage testPage;

}