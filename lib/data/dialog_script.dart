import 'package:flutter/cupertino.dart';
import 'package:mydness/data/outside_link.dart';

enum Gender { Male, Female }

class DialogContext {

  final DialogStep dialogRoot;
  final Gender gender;

  DialogContext({this.dialogRoot, this.gender});
}

class DialogOption {

  final String title;
  final DialogStep next;

  DialogOption({
    @required this.title,
    @required this.next
  }): assert(title != null),
      assert(next != null);
}

class DialogStep {

  final List<DialogMessage> messages;
  final List<DialogOption> options;

  DialogStep({
    @required this.messages,
    this.options
  }): assert(messages != null);

}

class DialogMessage {

  final String text;
  final Duration delay;
  final List<OutsideLink> links;
  final String imageUrl;

  DialogMessage({
    @required this.text,
    this.links,
    this.delay = const Duration(seconds: 1),
    this.imageUrl
  }): assert(text != null);
}


get commonDialog =>
  DialogStep(
    messages: [
      DialogMessage(text: "Привет. Какими судьбами?")
    ],
    options: [
      DialogOption(
        title: "Почиллить хочу",
        next: DialogStep(
          messages: [
            DialogMessage(
              text: "Для тебя у меня есть шикарная прога. "
              "Видел в кино, как на нескольких мониторах всякая разноцветная дичь творится? "
              "Сделай у себя так же!"
            ),
            DialogMessage(
              text: "Вот мой сайт. Там научу как пользоваться. "
                "Он правда еще в разработке, но ты пиши в личку, если что не понятно.",
              links: [ MySocials.ya_radio ]
            ),
            DialogMessage(
              delay: Duration(milliseconds: 300),
              text: "PS. не бойся, что не https. Это честно мой сайт))"
            ),
            DialogMessage(
              delay: Duration(milliseconds: 600),
              text: "А, ну еще можешь заценить мое радио на Яндекс.Радио",
              links: [ MySocials.ya_radio ]
            ),
            DialogMessage(
              text: "Но учти, это на свой страх и риск делаешь. Мои вкусы, знаешь ли, весьма специфичны)",
              imageUrl: "https://pbs.twimg.com/media/C7qWQ1RVsAAZ710?format=jpg&name=medium"
            ),
          ]
        )
      ),
    ]
  );