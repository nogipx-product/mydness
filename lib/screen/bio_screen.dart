import 'package:flutter/material.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:loggable_mixin/loggable_mixin.dart';
import 'package:mydness/config/ui.dart';
import 'package:mydness/data/outside_link.dart';
import 'package:mydness/extensions/url_extension.dart';
import 'package:mydness/widget/fa_chip.dart';
import 'package:responsive_builder/responsive_builder.dart';

class BioScreen extends StatelessWidget with Loggable {
  final _scaffold = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Padding(
                padding: GoogleInset.header_narrow,
                child: CircleAvatar(
                  radius: 64,
                  backgroundImage: NetworkImage(MydnessAsset.bio_avatar,
                    scale: .4
                  ),
                ),
              ),
              Padding(
                padding: GoogleInset.header_tiny,
                child: Text(MydnessText.about_me,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6
                ),
              ),
              _buildLinkBlock("Социальные сети", MySocials.active),
              _buildLinkBlock("Работа", WorkRelated.active),
            ],
          ),
        ),
      )
    );
  }

  Widget _buildLinkBlock(String title, List<OutsideLink> links) {
    return ResponsiveBuilder(
      builder: (context, sizing) {
        double width;
        if (sizing.isDesktop)
          width = MediaQuery.of(context).size.width * .25;
        if (sizing.isTablet)
          width = MediaQuery.of(context).size.width * .6;

        return Container(
          padding: sizing.isDesktop
            ? GoogleInset.header_narrow
            : GoogleInset.header_tiny,
          width: width,
          child: OutsideLinksWidget(
            links: links,
            title: title,
          ),
        );
      },
    );
  }
}

class OutsideLinksWidget
  extends StatelessWidget
  with Loggable {

  final String title;
  final List<OutsideLink> links;

  OutsideLinksWidget({Key key,
    this.title,
    @required this.links
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<OutsideLink> sorted = links
      .where((element) => element.icon != null)
      .toList();
    sorted.addAll(links
      .where((element) => element.icon == null)
      .toList());

    return GoogleSettingArea(
      title: title,
      child: ListView.separated(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.all(0),
        primary: false,
        itemCount: sorted.length,
        separatorBuilder: (_, __) => GoogleSettingDivider(),
        itemBuilder: (context, index) {
          final social = sorted[index];
          return _buildChip(context, social);
        },
      ),
    );
  }

  Widget _buildChip(context, OutsideLink social) {
    return FAChip(
      dark: true,
      icon: social.icon,
      iconSize: 20,
      label: Text(social.name,
        softWrap: true,
        style: Theme.of(context).textTheme.subtitle1
      ),
      color: social.color,
      borderRadius: 0,
      hideEmptyIcon: false,
      onTap: () async {
        social.link.launchUrl();
      },
      padding: GoogleInset.narrow,
    );
  }
}
