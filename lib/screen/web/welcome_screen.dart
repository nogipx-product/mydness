import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cross_platform/flutter_cross_platform.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:mydness/config/ui.dart';
import 'package:mydness/data/outside_link.dart';
import 'package:mydness/extensions/url_extension.dart';
import 'package:mydness/widget/fa_chip.dart';
import 'package:mydness/widget/web/corner_button.dart';
import 'package:responsive_builder/responsive_builder.dart';

class WelcomeScreen extends StatelessWidget with ScreenSplitMixin {

  @override
  Widget build(BuildContext context) {
    return CornerButtonsOverlay(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: screenVariant(context)
        ),
      ),
    );
  }

  List<Widget> _buildCredential(context) {
    return [
      FAChip(
        widgetIcon: FlutterLogo(),
        color: Color(0xff61d1fd),
        onTap: () => "https://flutter.dev/".launchUrl(),
        dark: true,
        label: Text("powered with Flutter",
          style: Theme.of(context).accentTextTheme.bodyText2,
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("nogipx@${DateTime.now().year}",
          style: Theme.of(context).accentTextTheme.bodyText2,
        ),
      ),
    ];
  }

  @override
  Widget desktop(context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 18,
          child: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                WelcomeTitle(),
                WelcomeInfo(),
                Padding(
                  padding: GoogleInset.header_narrow,
                  child: WelcomeLinks(),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.all(MaterialSpace.tiny),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: _buildCredential(context),
            ),
          )
        )
      ],
    );
  }

  @override
  Widget mobile(context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(MaterialSpace.tiny),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              WelcomeTitle(),
              WelcomeInfo(),
              WelcomeLinks(),
            ],
          ),
          Container(
            padding: EdgeInsets.all(MaterialSpace.tiny),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: _buildCredential(context),
            ),
          )
        ],
      )
    );
  }

  @override
  Widget tablet(context) => mobile(context);

  @override
  Widget watch(context) => mobile(context);
}

class WelcomeLinks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizing) {
        final screenWidth = MediaQuery.of(context).size.width;
        return Container(
          padding: GoogleInset.header_narrow,
          width: sizing.isDesktop ? screenWidth * .5 : screenWidth,
          child: Wrap(
            spacing: MaterialSpace.tiny,
            runSpacing: MaterialSpace.tiny,
            alignment: WrapAlignment.center,
            children: [
              MySocials.gitlab, MySocials.telegram, MySocials.instagram,
              WorkRelated.hh, WorkRelated.codewars
            ].map((link) => FAChip(
              icon: link.icon,
              iconSize: 24,
              label: Text(link.name.split(" ")[0],
                style: Theme.of(context).accentTextTheme.headline6.apply(
                  fontWeightDelta: 500
                ),
              ),
              hoverOpacity: .3,
              color: link.color,
              onTap: () => link.link.launchUrl(),
              dark: true,
            )).toList(),
          ),
        );
      },
    );
  }
}



// ignore: must_be_immutable
class WelcomeInfo extends StatelessWidget with ScreenSplitMixin {

  final birthday = DateTime.parse("20001004");
  int age;

  @override
  Widget build(BuildContext context) {
    age = DateTime.now().difference(birthday).inDays ~/ 365;
    return screenVariant(context);
  }

  @override
  Widget desktop(context) => _buildText(
    textStyle: Theme.of(context).textTheme.headline4
  );

  @override
  Widget mobile(context) => _buildText(
    textStyle: Theme.of(context).textTheme.headline6
  );

  @override
  Widget tablet(context) => mobile(context);

  @override
  Widget watch(context) => watch(context);

  Widget _buildText({TextStyle textStyle}) {
    return Container(
      padding: GoogleInset.header_narrow,
      child: Text("$age ${MydnessText.welcome_subtitle}",
        textAlign: TextAlign.center,
        style: textStyle
      ),
    );
  }
}


class WelcomeTitle extends StatelessWidget with ScreenSplitMixin {
  @override
  Widget build(BuildContext context) {
    return screenVariant(context);
  }

  @override
  Widget desktop(context) {
    return _buildText(context, style: Theme.of(context).primaryTextTheme.headline1);
  }

  @override
  Widget mobile(context) {
    return _buildText(context, style: Theme.of(context).primaryTextTheme.headline2);
  }

  @override
  Widget tablet(context) => mobile(context);

  @override
  Widget watch(context) => mobile(context);

  Widget _buildText(context, {TextStyle style}) => Text(
    MydnessText.welcome_title,
    textAlign: TextAlign.center,
    style: style?.copyWith(
      letterSpacing: 1.3,
      fontWeight: FontWeight.bold
    ),
  );
}

