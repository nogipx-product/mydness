import 'package:url_launcher/url_launcher.dart';

extension UrlString on String {
  Future launchUrl() async {
    if (await canLaunch(this)) {
      return launch(this);
    } else {
      print('Could not launch $this');
    }
  }
}